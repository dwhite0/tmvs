--Copyright 2010 David White

--This file is part of Tiny Machine Visual Simulator.

--Tiny Machine Visual Simulator is free software: you can redistribute 
--it and/or modify it under the terms of the GNU General Public License 
--as published by the Free Software Foundation, either version 3 of the 
--License, or (at your option) any later version.

--Tiny Machine Visual Simulator is distributed in the hope that it 
--will be useful, but WITHOUT ANY WARRANTY; without even the implied 
--warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
--See the GNU General Public License for more details.

--You should have received a copy of the GNU General Public License
--along with Tiny Machine Visual Simulator.  If not, see 
--<http://www.gnu.org/licenses/>.

module Constants where

-- Printing

tabSpace = (4 :: Int)

-- Code generation

initFunLabel = (2 :: Int)

initGP = (0 :: Int)

ofpFO = (0 :: Int)
retFO = (-1 :: Int)
initFO = (-2 :: Int)

pc = (7 :: Int)
gp = (6 :: Int)
fp = (5 :: Int)
ac = (0 :: Int)
ac1 = (1 :: Int)

